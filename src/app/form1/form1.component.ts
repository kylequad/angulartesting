import { Component, OnInit } from '@angular/core';
import { FormControl, SelectControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class Form1Component implements OnInit{

  form1Control = new FormControl('');

  constructor() { }

  ngOnInit(): void {
    this.setText();
  }

  setText() {
    this.form1Control.setValue('Default Text');
  }
}
