import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Form1Component } from './form1.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('Form1Component', () => {
  let component: Form1Component;
  let fixture: ComponentFixture<Form1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Form1Component ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Form1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create form', () => {
    const fixture = TestBed.createComponent(Form1Component);
    const form = fixture.debugElement.componentInstance;
    expect(form).toBeTruthy();
  });

  it('should have correct default value', () => {
    expect(fixture.componentInstance.form1Control.value).toEqual('Default Text');
  });

  it('should update the value of the input field', () => {
    const input = fixture.nativeElement.querySelector('#testinput');
  
    input.value = 'Test';
    input.dispatchEvent(new Event('input'));
  
    expect(fixture.componentInstance.form1Control.value).toEqual('Test');
  });

  it('should render the new value after updating', () => {
    const input = fixture.nativeElement.querySelector('#testinput');
    const output = fixture.nativeElement.querySelector('#display');

    expect(output.textContent.trim()).toEqual('Entered Text: Default Text');

    input.value = 'Test';
    input.dispatchEvent(new Event('input'));

    fixture.detectChanges();

    //fixture.whenStable().then(() => {
    expect(output.textContent.trim()).toEqual('Entered Text: Test');
    //});
  });
});
